+++
tags = ["tutorial", "series", "hugo"]
title = "Les commandes Git"
description = "A tutorial on how to use and customize the Hugo-Geo theme"
index = true
highlight = true
+++

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquet molestie mauris nec tempor. Sed non lacus massa. Aliquam ut convallis velit. Etiam dictum mollis mi, a vestibulum ex suscipit ut. Proin viverra lacinia urna at interdum. Sed eu quam sit amet metus fermentum pharetra id a sem. Pellentesque lorem dolor, venenatis sed felis non, fringilla ornare felis. Donec nisi lorem, dignissim id interdum vel, cursus malesuada quam. Integer tempus consectetur tellus, nec sagittis est mattis vel. Curabitur sit amet aliquet dolor, et placerat ipsum.

Pellentesque iaculis porttitor lobortis. Ut condimentum cursus libero, dictum accumsan felis congue et. Proin tellus ligula, sodales ut rhoncus sed, scelerisque sit amet turpis. Donec aliquam purus ac enim laoreet, ac viverra erat feugiat. Etiam quis dictum massa, vel molestie sapien. Sed ut euismod magna. Sed at quam augue. Donec eu tincidunt quam, ut venenatis nulla. Donec feugiat ullamcorper massa. Phasellus augue lorem, tincidunt aliquam est at, finibus lacinia ex. Cras sed risus pharetra dolor viverra vulputate quis sed nisl.
