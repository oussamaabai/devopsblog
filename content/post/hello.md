+++
date = "2015-07-28T09:32:45-04:00"
draft = false
title = "Welcome to Hugo-Geo!"
description = "A tutorial on how to use and customize the Hugo-Geo theme"
tags = ["blog", "hugo"]
index = true
highlight = true
+++


Welcome to hugo-geo, a resposive theme for the [Hugo static site generator.](http://gohugo.io) Hugo is designed for `post` and `tutorial` sections, and is perfect for personal
portfolio or blog sites.
