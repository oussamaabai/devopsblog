+++
date = "2015-07-28T09:32:45-04:00"
draft = false
title = "Ansible Basics and Best Practices"
description = "Ansible Basics and Best Practices"
tags = ["Ansible", "Best-Practices"]
index = true
highlight = true
+++




About Ansible
Ansible is an open-source automation engine which can be used for automating server provisioning, configuration management, and application deployment, intra-service orchestration and many other IT needs.

Ansible does not follow master and agent model i.e. It does not use agents and no additional custom security infrastructure, so it’s easy to deploy - and most importantly, it uses a very simple language YAML for Ansible Playbooks which allows you to describe your automation jobs in a simple to understand way and approaches plain English.


Installing and configuring Ansible

In this post we are using RedHat 7 server.
``` code
[root@techguru ~]# yum -y install ansible  
[root@techguru ~]# ansible --version  
ansible 2.3.0.0  
  config file = /etc/ansible/ansible.cfg  
  configured module search path = Default w/o overrides
  python version = 2.7.5 (default, Aug  2 2016, 04:20:16) [GCC 4.8.5 20150623 (Red Hat 4.8.5-4)]
[root@techguru ~]#
```
Ansible Inventory File
Ansible Inventory file is a simple ini file that contains a list of servers on which the ansible commands will get executed. The default location of the inventory file is /etc/ansible/hosts.

Below is a sample of the inventory file.

[root@techguru-lab1 ~]# cat /etc/ansible/hosts 
 # remote machine
techguru-lab2.cloud
[root@techguru-lab1 ~]#
By default Ansible connects to remote machine over SSH, you can use passwords to connect to remote machine, but SSH keys with ssh-agent are one of the best ways to use Ansible.
Note: Root logins are not required, you can login as any user, and then su or sudo to any user.

Ansible Adhoc command examples
Ansible works by connecting to your nodes (remote machine) and pushing out small programs, called “Ansible modules” to them. In the below examples, we have used Ansible’s ping module.
For a complete list of all Ansible Modules refer to this link.

Lab1 machine sends a ping request to Lab2 machine. The result is a success and Lab2 responds with a pong message as shown in the output.

Running Ad-hoc command using ssh password
[root@techguru-lab1 ~]# ansible all -m ping --ask-pass 
SSH password: 
techguru-lab2.cloud | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
[root@techguru-lab1 ~]#
Note:
-m is a switch to specify an Ansible module.
‘all’ indicates that the ansible command should be run on all the hosts in the inventory file.

Running Ad-hoc coomand after adding ssh key (passwordless authentication)
[root@techguru-lab1 ~]# ansible all -m ping 
techguru-lab2.cloud | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
[root@techguru-lab1 ~]#
Ansible Playbooks
Adhoc commands are useful when you want to perform some quick action on the hosts, one line commands such as checking ping response or shutting down some machines etc.

But the true power of Ansible is in its Playbooks. These Ansible Playbooks are useful for configuration Management, Application deployments etc.

A Playbook is composed of one or more plays and plays consists of multiple tasks.

Below is the example of a playbook, Directory layout for playbook we are using here is based on best practices suggested by Ansible.

[root@techguru-lab1 sample-playbooks]# tree
.
├── env
│   └── hosts
├── nginx.yaml
├── roles
│   └── web
│       └── nginx
│           ├── files
│           │   └── index.html
│           ├── handlers
│           ├── tasks
│           │   └── main.yaml
│           └── vars
└── site.yaml
 
8 directories, 5 files
[root@techguru-lab1 sample-playbooks]#
site.yaml - This file is the top level file which is used to include other playbooks. For example, here we have included nginx playbook.

[root@techguru-lab1 sample-playbooks]# cat site.yaml 
---
- include: nginx.yaml
[root@techguru-lab1 sample-playbooks]#
nginx.yaml - In this file we have defined group of hosts on which this playbook needs to be executed. Also during execution this file will call defined role, in this case web/nginx

[root@techguru-lab1 sample-playbooks]# cat nginx.yaml 
---
- hosts: webservers
  roles:
    - web/nginx
  tags: nginx,web
[root@techguru-lab1 sample-playbooks]#
roles/web/nginx/tasks/main.yaml - This file contains lists of tasks, which will get executed in the same sequence as defined in the file.

[root@techguru-lab1 sample-playbooks]# cat roles/web/nginx/tasks/main.yaml 
---
- name: Install the nginx package 
  apt: name=nginx state=present
 
- name: Create web root directory
  file:
    path: /var/www/html
    state: directory
    mode: 0755
 
- name: Copy custom index.html file 
  copy:
    src: index.html
    dest: /var/www/html/index.html
    owner: root
    group: root
    mode: 0644
 
- name: start the nginx service
  service: name=nginx state=started enabled=yes
[root@techguru-lab1 sample-playbooks]#
Command to execute ansible playbook.

[root@techguru-lab1 sample-playbooks]# ansible-playbook -i env/hosts site.yaml 
PLAY [webservers] ***********************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************************************************************************************************************
ok: [techguru-lab2.cloud]

TASK [web/nginx : Install the nginx package] *************************************************************************************************************************************************************************************************
changed: [techguru-lab2.cloud]
 
TASK [web/nginx : Create web root directory] *************************************************************************************************************************************************************************************************
ok: [techguru-lab2.cloud]
 
TASK [web/nginx : Copy custom index.html file] ***********************************************************************************************************************************************************************************************
changed: [techguru-lab2.cloud]
 
TASK [web/nginx : start the nginx service] ***************************************************************************************************************************************************************************************************
ok: [techguru-lab2.cloud]
 
PLAY RECAP ***********************************************************************************************************************************************************************************************************************************
techguru-lab2.cloud        : ok=5    changed=2    unreachable=0    failed=0   
 
[root@techguru-lab1 sample-playbooks]# 
References:
http://docs.ansible.com/ansible/playbooks_best_practices.html#directory-layout http://docs.ansible.com/ansible/modules_by_category.html

